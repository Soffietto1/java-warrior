package ru.itis.javawarrior.util;

/**
 * @author Damir Ilyasov
 */
public enum ActionEnum {
    MOVE_FORWARD,
    FLIP_FORWARD,
    SHOOT
}
