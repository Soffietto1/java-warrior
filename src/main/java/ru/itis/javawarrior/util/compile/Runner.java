package ru.itis.javawarrior.util.compile;

import java.util.List;

import ru.itis.javawarrior.util.ActionEnum;

public interface Runner {
    List<ActionEnum> main();
}
