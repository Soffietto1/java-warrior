package ru.itis.javawarrior.service;

/**
 * @author Damir Ilyasov
 */
public interface ActionService {

    void walk();

    void shoot();

    void jump();

}
