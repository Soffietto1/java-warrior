package ru.itis.javawarrior;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JavaWarriorApplication {

    public static void main(String[] args) {

        SpringApplication.run(JavaWarriorApplication.class, args);
        //Проверка пуша
    }
}
